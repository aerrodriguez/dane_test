import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AuthGuard } from './guards/authGuard';

const routes: Routes = [
{ path: '', redirectTo: 'users', pathMatch: 'full' },
{ path: 'users', component: UserListComponent, canActivate: [AuthGuard] },
{ path: 'form', component: UserFormComponent},
{ path: 'form/:id', component: UserFormComponent , canActivate: [AuthGuard]},
{ path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
