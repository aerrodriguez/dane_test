import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../model/user.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
/**
 * Componente para implementar las funcionalidades de creación y edición de usuarios
 * 
 */
export class UserFormComponent implements OnInit {

  genders = [
    { id: "Hombre", name: "Hombre" },
    { id: "Mujer", name: "Mujer" }
  ];

  user: User = {
    id: null,
    firstName: '',
    secondName: '',
    firstSurname: '',
    secondSurname: '',
    numberId: null,
    age: null,
    gender: '',
    email: '',
    cellPhone: null,
    state: false,
    password: null
  };
  message = '';

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.message = '';
    const id = this.route.snapshot.params.id;
    if (id) {
      this.editUser(this.route.snapshot.params.id);
    }
  }

  editUser(id: string): void {
    this.userService.get(id)
      .subscribe(
        data => {
          this.user = data;
        },
        error => {
          console.error(error);
        });
  }

  saveUser(): void {
    this.message = '';

    if (this.user.id) {
      this.saveEditedUser();
    } else {
      this.createNewUser();
    }
  }

  private createNewUser() {
    this.userService.create(this.user)
      .subscribe(
        response => {
          this.router.navigate(['/users']);
        },
        error => {
          console.error(error);
          this.message = 'Ocurrio un error guardando el usuario';
        });
  }

  private saveEditedUser() {
    this.userService.update(this.user.id, this.user)
      .subscribe(
        response => {
          this.router.navigate(['/users']);
        },
        error => {
          console.error(error);
          this.message = 'Ocurrio un error actualizando el usuario';
        });
  }
}
