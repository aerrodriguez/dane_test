import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
/**
 * Componente que lista los usarios existentes, además permite la previsualización de cierta información de un usuario seleccionado,
 * dando la opción de editar, eliminar o activar/inactivar un usuario
 * 
 */
export class UserListComponent implements OnInit {

  users?: User[];
  selected?: User;
  currentIndex: number = -1;
  message: string = '';

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.list()
      .subscribe(
        data => {
          this.users = data;
        },
        error => {
          console.error(error);
        });
  }


  setSelected(user: User, index: number): void {
    if (this.selected && this.selected.numberId == user.numberId) {
      this.selected = undefined;
      this.currentIndex = -1;
    } else {
      this.selected = user;
      this.currentIndex = index;
    }
  }

  deleteUser(): void {
    if (!this.selected) {
      return;
    }

    this.userService.delete(this.selected.id)
      .subscribe(
        response => {
          this.refreshList();
        },
        error => {
          console.error(error);
        });
  }

  refreshList(): void {
    this.getUsers();
    this.selected = undefined;
    this.currentIndex = -1;
  }

  activateUser(activated: boolean): void {
    if (!this.selected) {
      return;
    }

    const data = {
      id: this.selected.id,
      state: activated
    };

    this.message = '';

    this.userService.activateUser(this.selected.id, data)
      .subscribe(
        response => {
          if (this.selected) {
            this.selected.state = activated;
          }
        },
        error => {
          console.error(error);
        });
  }

}
