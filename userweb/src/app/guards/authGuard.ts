
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';

/**
 * Clase que permite realizar el filtro de las autorizaciónes de las páginas
 */
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
        private userServices: UserService) { }

       canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const usersito = localStorage.getItem('currentUser');
        if (localStorage.getItem('currentUser')!=null) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

}