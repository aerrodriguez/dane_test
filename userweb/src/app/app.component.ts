import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private userService: UserService,
  ) { }

  title = 'userweb';
  user = localStorage.getItem('currentUser');

  logout() {
    this.userService.logout();
  }
}
