import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../model/user.model';


const baseUrl = 'http://localhost:8080/api/users';

@Injectable({
  providedIn: 'root'
})

/**
 * Clase que implementa el llamado a los servicios rest de administración de usuarios,
 * también tiene la responsabilidad de realizar la autenticación y el logout de la aplicación
 */
export class UserService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  list(): Observable<any> {
    return this.http.get(baseUrl);
  }

  get(id: string): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: User): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: User): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  activateUser(id: number, data: User): Observable<any> {
    return this.http.put(`${baseUrl}/${id}/activate`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  login(numberId, password) {
    return this.http.post<User>(`${baseUrl}/authenticate`, { numberId, password })
      .pipe(map(user => {
        if(user){
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.userSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }
}
