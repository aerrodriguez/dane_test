/**
 * Modelo usado en la vista para creación o edición de usuarios
 */
export class User {

    id?: number;
    firstName?: string;
    secondName?: string;
    firstSurname?: string;
    secondSurname?: string;
    numberId?: number;
    age?: number;
    gender?: string;
    email?: string;
    cellPhone?: number;
    state?: boolean;
    password?: string;

}