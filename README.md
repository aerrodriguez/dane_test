**PROYECTO DE ADMINISTRACIÓN DE USUARIOS**
_(Prueba técnica como desarrollador para el DANE)_


Estas instrucciónes le permitiran ejecutar el proyecto. El proyecto consta de dos componentes. El primero corresponde al servidor, implementado con spring boot y el segundo corresponde al cliente, desarrollado con Angular.

**Herramientas utilizadas:** 📋

- Angular versión 11
- Oracle Express Edition 18c
- Spring Boot 2.3.1
- Maven
- Java 1.8

**Ejecución:**

Para realizar el despliegue del servidor puede seguir los siguientes pasos:
1. Empaquetar el servidor: ejecutar mvn package dentro del proyecto "usermanager"
2. Ejecutar java -jar path/{donde se ubique el archivo}/usermanager-0.0.1-SNAPSHOT.jar gov.dane.usermanager.Application 

Para probar el cliente:
1. Ubiquese dentro del proyecto userweb
2. ejecutar el comando ng serve (Debe tener instalado angular-cli)



**Bugs conocidos y opciones de mejora:**
- Menú del banner no se refresca de manera inmediata al momento de registrar el usuario en sesión.
- Se puede mejorar la seguridad implementando la encriptación de la contraseña y a nivel de comunicación, manejado tokens.
- Se puede mejorar la distribución de los componentes visuales
- Se puede adicionar un componente para el manejo de mensajes y alertas.



