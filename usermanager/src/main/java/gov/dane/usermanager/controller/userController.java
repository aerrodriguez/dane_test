package gov.dane.usermanager.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.dane.usermanager.model.User;
import gov.dane.usermanager.model.UserLogin;
import gov.dane.usermanager.model.UserRequest;
import gov.dane.usermanager.service.UserService;
/**
 * Servicio rest de spring boot
 * @author andres
 * 
 * Se define la interfaz para realizar operciones de CRUD para usuarios,
 * además cuenta con unos métodos para la autenticación
 * 
 * "Se define la url CrossOrigin para poder realizar las pruebas"
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/users")
public class userController {
	
	private final UserService userService;
	
	@Autowired
	public userController(UserService userService) {
		this.userService = userService;
	}
	
	
	@GetMapping("{id}")
    public ResponseEntity<User> user(@PathVariable Integer id) {
        Optional<User> user = userService.findById(id);
        return user.map(ResponseEntity::ok)
                   .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public List<User> list() {
       return userService.getAll();
    }

    @PostMapping
    public void save(@RequestBody UserRequest request) {
        userService.save(request);
    }

    @PutMapping("{id}/activate")
    public void activateUser(@PathVariable Integer id, @RequestBody UserRequest request) {
        userService.activate(id, request);
    }

    @PutMapping("{id}")
    public void update(@PathVariable Integer id, @RequestBody UserRequest request) {
        Optional<User> user = userService.findById(id);
        if (user.isPresent()) {
            userService.update(id, request);
        } else {
            userService.save(request);
        }
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }
    
    @PostMapping("/authenticate")
    public ResponseEntity<User> authenticate(@RequestBody UserLogin userLogin){
    	User user = userService.login(userLogin.getNumberId(), userLogin.getPassword());
    	return ResponseEntity.ok(user);
    }

}
