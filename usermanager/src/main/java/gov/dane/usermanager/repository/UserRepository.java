package gov.dane.usermanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gov.dane.usermanager.model.User;

/**
 * Clase para el acceso de datos extendiendo de la clase JpaRepository para obtener la funcionalidad de CRUD
 * 
 * @author andres
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	 User findByNumberIdAndPassword(Integer numberId, String password);
}
