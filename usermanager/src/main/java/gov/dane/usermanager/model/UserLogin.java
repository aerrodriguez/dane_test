package gov.dane.usermanager.model;

/**
 * modelo para transportar la información relacionada con las credenciales de autenticación
 * @author andres
 *
 */
public class UserLogin {
	
	private Integer numberId;
	private String password;
	
	public Integer getNumberId() {
		return numberId;
	}
	public void setNumberId(Integer numberId) {
		this.numberId = numberId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
