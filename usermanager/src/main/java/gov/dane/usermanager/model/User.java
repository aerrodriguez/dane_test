package gov.dane.usermanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entidad JPA que representa el mapeo de la tabla de usuario
 * @author andres
 *
 */
@Entity
@Table(name = "usuario")
public class User {
	
	/**
	 * primer nombre del usuario
	 */
	@Id
	@GeneratedValue
	private Integer id;
	
	/**
	 * primer nombre del usuario
	 */
	@NotNull
	@Column(name = "primer_nombre")
	private String firstName;
	/**
	 * optional
	 */
	@Column(name = "segundo_nombre")
	private String secondName;
	/**
	 * required
	 */
	@NotNull
	@Column(name = "primer_apellido")
	private String firstSurname;
	/**
	 * required
	 */
	@NotNull
	@Column(name = "segundo_apellido")
	private String secondSurname;
	/**
	 * required
	 */
	@NotNull
	@Column(name = "numero_identificacion", unique = true)
	private Integer numberId;
	/**
	 * optional
	 */
	@Column(name = "edad")
	private Integer age;
	/**
	 * required
	 */
	@NotNull
	@Column(name = "genero")
	private String gender;
	/**
	 * optional
	 */
	@Column(name = "email")
	private String email;
	/**
	 * optional
	 */
	@Column(name = "numero_celular")
	private Long cellPhone;
	
	@Column(name = "estado")
	private Boolean state;
	
	@NotNull
	private String password;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getFirstSurname() {
		return firstSurname;
	}
	public void setFirstSurname(String firstSurname) {
		this.firstSurname = firstSurname;
	}
	public String getSecondSurname() {
		return secondSurname;
	}
	public void setSecondSurname(String secondSurname) {
		this.secondSurname = secondSurname;
	}
	public Integer getNumberId() {
		return numberId;
	}
	public void setNumberId(Integer numberId) {
		this.numberId = numberId;
	}
	public Integer getAge() {
		return age;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(Long cellPhone) {
		this.cellPhone = cellPhone;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
