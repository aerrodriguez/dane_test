package gov.dane.usermanager.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gov.dane.usermanager.model.User;
import gov.dane.usermanager.model.UserRequest;
import gov.dane.usermanager.repository.UserRepository;

/**
 * Servicio para gestionar la lógica de negoció de la administración de usuarios, permitiendo que el controlador no implemente dicha lógica 
 * 
 * @author andres
 *
 */
@Service
public class UserService {
	
	private final UserRepository userRepository;
	
	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	
	
	public Optional<User> findById(Integer id) {
		return userRepository.findById(id);
	}

	public void activate(Integer id, UserRequest request) {
		Optional<User> user = findById(id);
		if (user.isPresent()) {
			User p = user.get();
			p.setState(request.getState());
			userRepository.save(p);
		}
	}

	public void save(UserRequest request) {
		User user = new User();
		user.setAge(request.getAge());
		user.setCellPhone(request.getCellPhone());
		user.setEmail(request.getEmail());
		user.setFirstName(request.getFirstName());
		user.setFirstSurname(request.getFirstSurname());
		user.setGender(request.getSecondName());
		user.setSecondName(request.getSecondName());
		user.setSecondSurname(request.getSecondSurname());
		user.setNumberId(request.getNumberId());
		user.setPassword(request.getPassword());
		userRepository.save(user);
	}

	public void update(Integer id, UserRequest request) {
		Optional<User> user = findById(id);
		if (user.isPresent()) {
			User userToUpdate = user.get();
			userToUpdate.setAge(request.getAge());
			userToUpdate.setCellPhone(request.getCellPhone());
			userToUpdate.setEmail(request.getEmail());
			userToUpdate.setFirstName(request.getFirstName());
			userToUpdate.setGender(request.getSecondName());
			userToUpdate.setSecondName(request.getSecondName());
			userToUpdate.setSecondSurname(request.getSecondSurname());
			userToUpdate.setNumberId(request.getNumberId());
			userToUpdate.setPassword(request.getPassword());
			userRepository.save(userToUpdate);
		}
	}

	public List<User> getAll() {
		return userRepository.findAll();
	}

	public void delete(Integer id) {
		Optional<User> user = findById(id);
		user.ifPresent(userRepository::delete);
	}
	
	public User login(Integer numberId, String password) {
		return userRepository.findByNumberIdAndPassword(numberId, password);
	} 
}
